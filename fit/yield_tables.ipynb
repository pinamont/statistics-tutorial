{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "e9f7acec",
   "metadata": {},
   "source": [
    "# Understanding fit results: yield tables\n",
    "\n",
    "_Valerio Ippolito - INFN Sezione di Roma_\n",
    "\n",
    "Once you have your signal plus background model, is there anything better than looking at how it works on data?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d6ac81c6",
   "metadata": {},
   "source": [
    "Let's identify first of all which workspace we want to look at"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1c960120",
   "metadata": {},
   "outputs": [],
   "source": [
    "inputFile = TString(\"../ws/ATLASIT_prova_combined_ATLASIT_prova_model.root\");\n",
    "workspaceName = TString(\"combined\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e763763d",
   "metadata": {},
   "source": [
    "Since a workspace file might contain in principle multiple configurations of the statistical model, let's make sure we specify which ModelConfig we want to use:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c96832fb",
   "metadata": {},
   "outputs": [],
   "source": [
    "modelConfigName = TString(\"ModelConfig\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a8520fc1",
   "metadata": {},
   "source": [
    "We must also decide which data we want to look at. Real data? Asimov data? Something else?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bb7f465b",
   "metadata": {},
   "outputs": [],
   "source": [
    "dataName = TString(\"obsData\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3a0457c7",
   "metadata": {},
   "source": [
    "This code will produce output, and output should be stored in a folder and labeled with a nickname which _tags_ the workspace (e.g. the mass of the resonance you are looking at, etc.):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4e783c2a",
   "metadata": {},
   "outputs": [],
   "source": [
    "workspaceTag = TString(\"my_test\");\n",
    "outputFolder = TString(\"tmp\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fb265d66",
   "metadata": {},
   "source": [
    "We want to:\n",
    "- perform the fit using only few of the regions (something everyone calls either signal region[s] SR, or control regions CR)\n",
    "- evaluate the output of the fit in a possibly disjoint set of regions (e.g. SR expectation after B-only fit in CR)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6e25e766",
   "metadata": {},
   "outputs": [],
   "source": [
    "evalRegions = TString(\"ljets_Mbb_ge6jge4b\");\n",
    "fitRegions = TString(\"ljets_HThad_5j3b\");\n",
    "debugLevel = 2;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "abd43058",
   "metadata": {},
   "outputs": [],
   "source": [
    "//!cd ../CommonStatTools; mkdir build; cd build; cmake ..; cd -"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "58d9dfb5",
   "metadata": {},
   "source": [
    "We'll use the `HistFactoryInspector` class from CommonStatTools to do this. We are running from a Jupyter notebook, so we'll have to include things manually here - but if you prefer, you can run from the command line (check `CommonStatTools/test.sh` for an example, the code is called `getHFtables.C`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "57a72cf0",
   "metadata": {},
   "outputs": [],
   "source": [
    "#include \"../CommonStatTools/HistFactoryInspector.h\" // must be in a separate cell, for some Jupyter reason "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "67f56895",
   "metadata": {},
   "outputs": [],
   "source": [
    "gROOT->LoadMacro(\"../CommonStatTools/RooExpandedFitResult.C+\");\n",
    "gROOT->LoadMacro(\"../CommonStatTools/HistFactoryInspector.C+\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d063f7ea",
   "metadata": {},
   "source": [
    "The big question is: do we want to fit using the S+B model, or to set all POIs to zero?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c9ed20db",
   "metadata": {},
   "outputs": [],
   "source": [
    "set_poi_zero = kTRUE"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f1d2a212",
   "metadata": {},
   "source": [
    "Here we go:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0cb25264",
   "metadata": {},
   "outputs": [],
   "source": [
    "EXOSTATS::HistFactoryInspector hf;\n",
    "hf.setDebugLevel(debugLevel);\n",
    "hf.setInput(inputFile, workspaceName, modelConfigName, dataName, \"\", set_poi_zero); // the string should be left empty - it's the name of the range of the observable to use, something we don't need\n",
    "hf.setEvalRegions(evalRegions);\n",
    "hf.setFitRegions(fitRegions);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "44b2b610",
   "metadata": {},
   "source": [
    "Now, we can get the yields before and after fit:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b89d99e7",
   "metadata": {},
   "outputs": [],
   "source": [
    "hf.getYields();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a805b5c0",
   "metadata": {},
   "source": [
    "Often, you want to check the pre- and post-fit impact of uncertainties (i.e. nuisance parameters), possibly on a subset of the overall set of samples:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10d46340",
   "metadata": {},
   "outputs": [],
   "source": [
    "samplesForImpact = TString(\"ttH,ttbar,singleTop\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bbe017bf",
   "metadata": {},
   "source": [
    "We do so with"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d92653a6",
   "metadata": {},
   "outputs": [],
   "source": [
    "hf.getImpacts(samplesForImpact);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dfc20dc5",
   "metadata": {},
   "outputs": [],
   "source": [
    "//.x ../CommonStatTools/getHFtables.C(input_file, workspace_name, modelconfig_name, data_name, workspaceTag, outputFolder, eval_regions, fit_regions, doYields, doImpacts, samplesForImpact, debugLevel)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4e06747d",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "ROOT C++",
   "language": "c++",
   "name": "root"
  },
  "language_info": {
   "codemirror_mode": "text/x-c++src",
   "file_extension": ".C",
   "mimetype": " text/x-c++src",
   "name": "c++"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
