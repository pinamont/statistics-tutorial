{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f5109bce",
   "metadata": {},
   "source": [
    "# Inspecting a RooWorkspace\n",
    "_Valerio Ippolito - INFN Sezione di Roma_\n",
    "\n",
    "What's inside a RooWorkspace created with HistFactory? Let's find out in two different ways.\n",
    "\n",
    "\n",
    "## With ROOT\n",
    "Is there a better way to understand what's inside a ROOT file than opening it? Let's see what has been created by `create_data/create_workspace.ipynb`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ecb94bc9",
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls ../ws"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6e09602d",
   "metadata": {},
   "source": [
    "The output folder of a HistFactory run usually contains many files, one of which is quite meaningful: it's the one with the name `combined`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b9cd69a7",
   "metadata": {},
   "outputs": [],
   "source": [
    "f = new TFile(\"../ws/ATLASIT_prova_combined_ATLASIT_prova_model.root\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "db88f956",
   "metadata": {},
   "outputs": [],
   "source": [
    "f->ls()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "92726e0e",
   "metadata": {},
   "source": [
    "The file contains:\n",
    "- the `RooWorkspace` object, i.e. our likelihood model and associated datasets\n",
    "- another object we shall neglect\n",
    "- two directories containing the input histograms we used to build the workspace, one per channel (statistically independent set of data, cf. previous steps of this tutorial)\n",
    "- the Measurement object representing what we created with create_workspace.ipynb, so effectively the specification of what is inside the Workspace\n",
    "\n",
    "In case you ever panick and don't remember what's inside a workspace, you may do"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "114be1e9",
   "metadata": {},
   "outputs": [],
   "source": [
    "ATLASIT_prova->PrintXML(\"blabla\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cf20662f",
   "metadata": {},
   "source": [
    "and then the `blabla` folder (which is created just in case) contains the XML files which specify, in some convoluted version of the English language, what's inside the workspace:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ff96057d",
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls blabla"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6afe0c3f",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat blabla/ATLASIT_prova.xml"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "583585a9",
   "metadata": {},
   "source": [
    "Now, back on the workspace, which is usually called `combined`. What's inside it? We check it with"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8572376a",
   "metadata": {},
   "outputs": [],
   "source": [
    "w = dynamic_cast<RooWorkspace*>(f->Get(\"combined\"));\n",
    "w->Print(\"\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14812809",
   "metadata": {},
   "source": [
    "The workspace is actually a collection of stuff:\n",
    "- variables (which can be parameters, observables, constants... anything!)\n",
    "- probability density functions (PDFs), i.e. mathematical functions of those variables (technically there is no distinction between observables and parameters at this stage)\n",
    "- functions, which - differently from PDFs - aren't supposed to be normalized to 1 (for example, a histogram can be seen as a function of an observable, whose integral is equal to a given number of events)\n",
    "- datasets, i.e. real data (the `Data` input histograms we gave when creating the workspace) and so-called Asimov data, which represent the overall _nominal_ background expectation;\n",
    "- embedded datasets, i.e. internal representations of histograms\n",
    "- parameter snapshots, i.e. \"named copies\" of the value (e.g. `9.888`) and settings (e.g. up and down error, `isConstant=kTRUE`) of a set of parameters\n",
    "- named sets, i.e. \"named sets\" of parameters, which share the same meaning - e.g. \"what's or what are the POI[s]\", \"which are the observables\", etc.\n",
    "- generic objects, like the notable HistFactory `ModelConfig` object, which represents how we should be putting together the overall likelihood model (PDF, data, and notion of who's the POI and who are the observable, the nuisance parameters and the constant parameters in our statistical task)\n",
    "\n",
    "Please note that the meaning of _nominal_ depends strongly on the default parameters which were set in the creation of the workspace. More specifically: normalization parameters (such as the signal strength, which is usually the POI, and the background normalization factors) are taken at their nominal value as declared in the workspace creation. Typical mistake: expecting the Asimov dataset to represent the _background-only hypothesis_ when you set the signal strength default value to `1`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fbae6cb4",
   "metadata": {},
   "source": [
    "We can for example identify what's the POI, and what are its default value and bounds:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "059a3639",
   "metadata": {},
   "outputs": [],
   "source": [
    "w->set(\"ModelConfig_POI\")->Print(\"v\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3ef7a95a",
   "metadata": {},
   "source": [
    "or understand what are the observables,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9ee3c38a",
   "metadata": {},
   "outputs": [],
   "source": [
    "w->set(\"observables\")->Print(\"v\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "96ff8a38",
   "metadata": {},
   "source": [
    "or we may do that via the ModelConfig object,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "56c31b27",
   "metadata": {},
   "outputs": [],
   "source": [
    "mc = dynamic_cast<RooStats::ModelConfig*>(w->obj(\"ModelConfig\"));\n",
    "mc->GetParametersOfInterest()->Print();\n",
    "mc->GetObservables()->Print();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7197a0f3",
   "metadata": {},
   "source": [
    "and inspect the variable info via"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8e8dd917",
   "metadata": {},
   "outputs": [],
   "source": [
    "w->var(\"mu_ttH\")->Print()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13ee7e8d",
   "metadata": {},
   "source": [
    "We can also perform some simple plotting, provided we choose the observables to look at. HistFactory workspaces always have two special ones:\n",
    "- `channelCat` represents the category the events fall in (i.e. it's like an enum representing which of the various channels events belong to - for two channels, you'll see two _Possible states_ this discrete variable can take)\n",
    "- `weightVar` represents instead the weight which may be associated to each event"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "70f34bfc",
   "metadata": {},
   "outputs": [],
   "source": [
    "w->cat(\"channelCat\")->Print(\"v\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26eb2ce9",
   "metadata": {},
   "source": [
    "Verbose printouts (`Print(\"v\")`) and tree printouts (`Print(\"t\")`) help nerds understand what's going on - how a variable or a function or in general any RooFit object is defined, which functions use it, and so on and so forth."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "534f15ea",
   "metadata": {},
   "source": [
    "## With CommonStatTools"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c8732f5c",
   "metadata": {},
   "source": [
    "The CommonStatTools package provides a simple tool to extract histograms out of the folder structure of the file containing the RooWorkspace, and put them out in a single ROOT file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "04434f20",
   "metadata": {},
   "outputs": [],
   "source": [
    "!python ../CommonStatTools/obtainHistosFromWS.py -i ../ws/ATLASIT_prova_combined_ATLASIT_prova_model.root -o validation_histos.root"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b817015b",
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "452fa874",
   "metadata": {},
   "outputs": [],
   "source": [
    "f_histo = new TFile(\"validation_histos.root\");\n",
    "f_histo->ls();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a662f3bb",
   "metadata": {},
   "outputs": [],
   "source": [
    "c = new TCanvas(\"c2\", \"c\", 800, 400);\n",
    "c->Divide(2, 1);\n",
    "\n",
    "// first channel\n",
    "c->cd(1);\n",
    "h_data = dynamic_cast<TH1D*>(f_histo->Get(\"ljets_HThad_5j3b_Data_regBin\"));\n",
    "h_ttH = dynamic_cast<TH1D*>(f_histo->Get(\"ljets_HThad_5j3b_ttH_regBin\"));\n",
    "h_ttbar = dynamic_cast<TH1D*>(f_histo->Get(\"ljets_HThad_5j3b_ttbar_regBin\"));\n",
    "h_singleTop = dynamic_cast<TH1D*>(f_histo->Get(\"ljets_HThad_5j3b_singleTop_regBin\"));\n",
    "\n",
    "h_ttH->SetLineColor(kRed);\n",
    "h_ttbar->SetLineColor(kBlue+1);\n",
    "h_singleTop->SetLineColor(kGreen-8);\n",
    "h_ttH->SetTitle(\"ttH\");\n",
    "h_ttbar->SetTitle(\"ttbar\");\n",
    "h_singleTop->SetTitle(\"single top\");\n",
    "h_data->SetTitle(\"data\");\n",
    "\n",
    "h_data->Draw(\"pe\");\n",
    "auto S_plus_B = new THStack(\"S_plus_B\", \"S plus B\");\n",
    "S_plus_B->Add(h_singleTop);\n",
    "S_plus_B->Add(h_ttbar);\n",
    "S_plus_B->Add(h_ttH);\n",
    "S_plus_B->Draw(\"hist same\");\n",
    "\n",
    "\n",
    "// second channel\n",
    "c->cd(2);\n",
    "h_data = dynamic_cast<TH1D*>(f_histo->Get(\"ljets_Mbb_ge6jge4b_Data_regBin\"));\n",
    "h_ttH = dynamic_cast<TH1D*>(f_histo->Get(\"ljets_Mbb_ge6jge4b_ttH_regBin\"));\n",
    "h_ttbar = dynamic_cast<TH1D*>(f_histo->Get(\"ljets_Mbb_ge6jge4b_ttbar_regBin\"));\n",
    "h_singleTop = dynamic_cast<TH1D*>(f_histo->Get(\"ljets_Mbb_ge6jge4b_singleTop_regBin\"));\n",
    "\n",
    "h_ttH->SetLineColor(kRed);\n",
    "h_ttbar->SetLineColor(kBlue+1);\n",
    "h_singleTop->SetLineColor(kGreen-8);\n",
    "h_ttH->SetTitle(\"ttH\");\n",
    "h_ttbar->SetTitle(\"ttbar\");\n",
    "h_singleTop->SetTitle(\"single top\");\n",
    "h_data->SetTitle(\"data\");\n",
    "\n",
    "h_data->Draw(\"pe\");\n",
    "auto S_plus_B2 = new THStack(\"S_plus_B\", \"S plus B\");\n",
    "S_plus_B2->Add(h_singleTop);\n",
    "S_plus_B2->Add(h_ttbar);\n",
    "S_plus_B2->Add(h_ttH);\n",
    "S_plus_B2->Draw(\"hist same\");\n",
    "\n",
    "\n",
    "c->cd(2)->BuildLegend();\n",
    "c->Draw();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dddae21d",
   "metadata": {},
   "source": [
    "You may sometimes need to compare different versions of the same workspace. In this case, a convenient way to identify changes in histograms is provided by CommonStatTools: let's first create a second mockup workspace,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0b733e81",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cp ../ws/ATLASIT_prova_combined_ATLASIT_prova_model.root another_mysterious_ws.root"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d6f1b2ce",
   "metadata": {},
   "source": [
    "and then we can visualize the differences between the histograms contained in the \"old\" (`-o`) and \"new\" (`-n`) files:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "742604d9",
   "metadata": {},
   "outputs": [],
   "source": [
    "!python ../CommonStatTools/compareHistos.py -o ../ws/ATLASIT_prova_combined_ATLASIT_prova_model.root -n another_mysterious_ws.root"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "defd1760",
   "metadata": {},
   "source": [
    "Of course there may be residual differences due to different settings in terms of normalization factors or normalization uncertainties, but that might be checked for example by comparing the XML files which can be extracted a-posteriori from the `Measurement` object in the file (`vimdiff` them!)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "ROOT C++",
   "language": "c++",
   "name": "root"
  },
  "language_info": {
   "codemirror_mode": "text/x-c++src",
   "file_extension": ".C",
   "mimetype": " text/x-c++src",
   "name": "c++"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
